const { urlContains } = require("wdio-wait-for");

describe('Demo test', ()=>{
    it('Check page title', async()=>{
        await browser.url('https://automate.test.io/projects/21/tasks/6856/test_case/automate_task_reports/4088/edit');
        await browser.pause(3000);
        await browser.maximizeWindow();
        const pageTitle = await browser.getTitle();
    
        await expect(pageTitle).toEqual('test IO Automate Space');
        await $('.btn').waitForDisplayed(10000);
        await $('.btn').click();
        await $('[href="/users/auth/epam_sso?redirect_subdomain="]').click();
        await $('[type="email"]').waitForDisplayed(15000);
        await $('[type="email"]').setValue('Olena_Leskiv@epam.com');
        
        await $('[type="submit"]').click();
        await $('[name="passwd"]').waitForDisplayed(15000);
        await $('[name="passwd"]').setValue('dl$PET8aaa');
        await $('[name="passwd"]').waitForDisplayed(15000);
        await $('[type="submit"]').click();
        //the authentication via mobile phone is done manually
        await browser.pause(20000);
        await expect(urlContains('https://automate.test.io/'));

    });
})